provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}
module "vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  name                 = var.vpc_name
  cidr                 = var.vpc_cidr
  azs                  = var.az
  public_subnets       = var.public_subnets
  enable_dns_hostnames = true
}

### Security group for squid
module "squidproxy-security-group" {
  source                   = "terraform-aws-modules/security-group/aws"
  name                     = var.squid_sg_name
  vpc_id                   = module.vpc.vpc_id
  ingress_cidr_blocks      = []
  ingress_rules            = []
  egress_rules             = ["all-all"]
  ingress_with_cidr_blocks = var.ingress_with_cidr_blocks_squid
}

### Security group for ssh
module "ssh-security-group" {
  source                   = "terraform-aws-modules/security-group/aws"
  name                     = var.ssh_sg_name
  vpc_id                   = module.vpc.vpc_id
  ingress_cidr_blocks      = []
  ingress_rules            = []
  egress_rules             = ["all-all"]
  ingress_with_cidr_blocks = var.ingress_with_cidr_blocks_ssh
}


### Network load balancer
module "nlb" {
  source             = "terraform-aws-modules/alb/aws"
  name               = var.nlb_name
  load_balancer_type = "network"
  vpc_id             = module.vpc.vpc_id
  subnets            = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  target_groups      = var.target_group
  http_tcp_listeners = var.http_listener
}

### Auto-scaling group
module "asg" {
  source                    = "terraform-aws-modules/autoscaling/aws"
  name                      = var.asg_name
  min_size                  = var.min_size
  max_size                  = var.max_size
  desired_capacity          = var.desired_capacity
  wait_for_capacity_timeout = 0
  health_check_type         = var.health_check_type
  vpc_zone_identifier       = [module.vpc.public_subnets[0], module.vpc.public_subnets[1]]
  target_group_arns         = module.nlb.target_group_arns


  # Launch template
  lt_name                = var.lt_name
  update_default_version = true
  use_lt                 = true
  create_lt              = true
  image_id               = var.image_id
  instance_type          = var.instance_type
  ebs_optimized          = false
  enable_monitoring      = true
  key_name               = var.key_name
  security_groups        = [module.squidproxy-security-group.security_group_id, module.ssh-security-group.security_group_id]
  user_data              = file("update.sh")
}

## Scaling policy
resource "aws_autoscaling_policy" "example" {
  name                   = var.policy_name
  autoscaling_group_name = module.asg.autoscaling_group_name
  policy_type            = var.policy_type

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = var.metric_type
    }
    target_value = var.target_value

  }
}

###Route53
data "aws_route53_zone" "selected" {
  name = var.domain_name
}

resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "www.${data.aws_route53_zone.selected.name}"
  type    = "A"

  alias {
    name                   = module.nlb.lb_dns_name
    zone_id                = module.nlb.lb_zone_id
    evaluate_target_health = true
  }
}

